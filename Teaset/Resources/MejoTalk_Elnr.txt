﻿お母様、調子はいかがですか？
犠牲は、私達だけでいいのです。
お母様、力比べをしませんか？
レイリー様がお母様に何かしてきたとしても、私達が守ってみせます。
父の愛が無くても、私達はお母様を愛していますよ